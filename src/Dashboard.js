import React, { useState, useEffect } from "react";
import DayWeather from "./Day";
import Cookies from "js-cookie";
import {useNavigate} from "react-router-dom";
import { Hearts } from  'react-loader-spinner'

const Dashboard = () => {
    const [country, setCountry] = useState("");
    const [city, setCity] = useState("");
    const [time, setTime] = useState("");
    const [weatherData, setWeatherData] = useState([]);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        const loggedIn = Cookies.get("loggedIn");
        if(!loggedIn) {
            navigate("/");
            alert("Please first log in!");
            return;
        }
        Cookies.remove("loggedIn");
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const { latitude, longitude } = position.coords;
                fetch(`https://api.weatherapi.com/v1/forecast.json?key=0886ad7865604d75a83231854231411&q=${latitude},${longitude}&days=5`)
                    .then((response) => response.json())
                    .then((data) => {
                        setWeatherData(data.forecast.forecastday);
                        setCountry(data.location.country);
                        setCity(data.location.name);
                        setTime(new Date(data.location.localtime).toLocaleTimeString('en-US', { hour: '2-digit', minute: '2-digit', hour12: true }));
                    })
                    .catch((error) => {
                        console.error("Error fetching weather data", error);
                    })
                    .finally(() => {
                        setTimeout(() => {
                            setLoading(false);
                        }, 1000);
                    })
            },
            (error) => {
                console.error("Error getting geolocation", error);
                setLoading(false);
            });
    }, []);

    if(loading) {
        return (
            <div id="loader">
                <Hearts
                    height="200"
                    width="200"
                    color="#5097e0"
                    ariaLabel="hearts-loading"
                    visible={true}
                />
            </div>
        );
    }

    return (
        <div id="weather-container">
            <header>
                <span>Meta Weather</span>
                <nav>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Map</a></li>
                        <li><a href="#">API</a></li>
                        <li><a href="#">About</a></li>
                    </ul>
                </nav>
            </header>
            <main>
                <div className="info">
                    <div id="location">
                        <span>{city}</span>
                        <span>{country}</span>
                    </div>
                    <div className="sun-info">
                        <div>
                            <span>Time:</span>
                            <span>{time}</span>
                        </div>
                        <div>
                            <span>Sunrise:</span>
                            <span>{weatherData[0].astro.sunrise}</span>
                        </div>
                        <div>
                            <span>Sunset:</span>
                            <span> {weatherData[0].astro.sunset}</span>
                        </div>
                    </div>
                </div>
                <div id="weather">
                    {weatherData.map((dayWeather, index) => (
                        <DayWeather key={index} dayWeather={{...dayWeather, index}} />
                    ))}
                </div>
            </main>
        </div>
    );
};

export default Dashboard;

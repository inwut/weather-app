import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import Cookies from "js-cookie";

const Login = () => {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [error, setMessage] = useState("");
    const navigate = useNavigate();

    const handleLogin = (e) => {
        e.preventDefault();
        if(login === "dasha@gmail.com" && password === "dasha2") {
            Cookies.set("loggedIn", "true");
            navigate("/dashboard");
        } else {
            setMessage("Invalid login or password!");
        }
    }

    return (
        <div id="login-container">
            <form id="login-form" onSubmit={handleLogin}>
                <h3>Log to Web App</h3>
                <hr/>
                <div id="form-data">
                    <label className="form-label" htmlFor="email-input">E-mail:</label>
                    <input id="email-input" type="email" onChange={(e) => setLogin(e.target.value)}/>
                    <label className="form-label" htmlFor="password-input">Password:</label>
                    <input id="password-input" type="password" onChange={(e) => setPassword(e.target.value)}/>
                    {error && <span className="error-message">{error}</span>}
                    <label id="remember-label">
                        <input id="remember-check" type="checkbox"/>
                        Remember me
                    </label>
                    <button id="submit-button" type="submit">Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login
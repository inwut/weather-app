import React from "react";

const DayWeather = (props) => {
    const dayWeather = props.dayWeather;
    return (
        <div className="day-weather">
            <h3>
                {dayWeather.index === 0
                    ? 'Today'
                    : dayWeather.index === 1
                        ? 'Tomorrow'
                        : new Date(dayWeather.date).toLocaleDateString('en-US', {
                            weekday: 'short',
                            month: 'short',
                            day: '2-digit',
                        }).replace(",", "")}
            </h3>
            <span className="condition">
      <img src={dayWeather.day.condition.icon} alt={dayWeather.day.condition.text}/>
                {dayWeather.day.condition.text.split(" ")[0]}
    </span>
            <span className="temp">Max: {dayWeather.day.maxtemp_c}°C</span>
            <span className="temp">Min: {dayWeather.day.mintemp_c}°C</span>
            <div>
                <span>Wind</span>
                <span>{dayWeather.day.maxwind_mph} mph</span>
            </div>
            <div>
                <span>Humidity</span>
                <span>{dayWeather.day.avghumidity}%</span>
            </div>
            <div>
                <span>Visibility</span>
                <span>{dayWeather.day.avgvis_miles} miles</span>
            </div>
            <div>
                <span>Pressure</span>
                <span>{dayWeather.hour[15].pressure_mb}mb</span>
            </div>
            <div>
                <span>Confidence</span>
                <span>{dayWeather.day.daily_chance_of_rain}%</span>
            </div>
        </div>
    );
}

export default DayWeather;
